let assert = chai.assert;
describe('Tabela', function() {
 describe('crtaj()', function() {
   it('should draw 1 rows when parameter are 1,1', function() {
     Tabela.crtaj(1,1);
     let tabele = document.getElementsByTagName("table");
     let tabela = tabele[tabele.length-1]
     let redovi = tabela.getElementsByTagName("tr");
     assert.equal(redovi.length, 1,"Broj redova treba biti 3");
   });
   it('should draw 3 columns in row when parameter are 3,3', function() {
       Tabela.crtaj(4,4);
       let tabele = document.getElementsByTagName("table");
       let tabela = tabele[tabele.length-1]
       let redovi = tabela.getElementsByTagName("tr");
       let kolone = redovi[1].getElementsByTagName("td");
       let brojPrikazanih = 0;
       for(let i=0;i<kolone.length;i++){
           let stil = window.getComputedStyle(kolone[i])
           if(stil.display!=='none') brojPrikazanih++;
       }
       assert.equal(brojPrikazanih, 1,"Broj kolona treba biti 1");
     });
 });
});
